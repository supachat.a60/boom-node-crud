var express = require('express')
var cors = require('cors')
const mysql = require('mysql2');
const PORT = process.env.PORT || 5000

const connection = mysql.createConnection({
  host:'157.245.59.56',
  port:'3366',
  user:'6205336',
  password:'6205336',
  database:'6205336'
});

var app = express()
app.use(cors())
app.use(express.json())

app.get('/emp', function (req, res, next) {
  connection.query(
    'SELECT * FROM `emp`',
    function(err, results, fields) {
      res.json(results);
    }
  );
})

app.get('/emp/:id', function (req, res, next) {
  const id = req.params.id;
  connection.query(
    'SELECT * FROM `emp` WHERE `id` = ?',
    [id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.post('/emp', function (req, res, next) {
  connection.query(
    'INSERT INTO `emp`(`ename`, `sname`, `department`, `sal`, `tel`) VALUES (?, ?, ?, ?, ?)',
    [req.body.ename, req.body.sname, req.body.department, req.body.sal, req.body.tel],
    function(err, results) {
      res.json(results);
    }
  );
})

app.put('/emp', function (req, res, next) {
  connection.query(
    'UPDATE `emp` SET `ename`= ?, `sname`= ?, `department`= ?, `sal`= ?, `tel`= ? WHERE id = ?',
    [req.body.ename, req.body.sname, req.body.department, req.body.sal, req.body.tel, req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.delete('/emp', function (req, res, next) {
  connection.query(
    'DELETE FROM `emp` WHERE id = ?',
    [req.body.id],
    function(err, results) {
      res.json(results);
    }
  );
})

app.listen(PORT, function () {
  console.log('CORS-enabled web server listening on port PORT'+PORT)
})
