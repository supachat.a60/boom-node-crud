import React, { useState, useEffect } from "react";
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';


import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Container } from '@mui/material';

export default function BasicGrid() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch("https://black-node-crud.herokuapp.com/emp")
      .then(res => res.json())
      .then(
        (result) => {
          setUsers(result);
        }
      )
  }, [])

  return (
    <Container maxWidth="lg" sx={{ mt: 2 }} > 
      <Grid container spacing={2}>
        {users.map(user => (
          <Grid item xs={12} md={4}>
            <Card>
              <CardHeader 
                avatar={
                  <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                    {user.ename[0]}
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings" >
                    <MoreVertIcon />
                  </IconButton>
                }
                title={user.id +
                   ' ' + user.ename + 
                   ' ' + user.sname }
                subheader={' Department: ' + user.department+ 
                ' Sal: ' + user.sal + 
                ' Tel: ' + user.tel}
              />
              <CardActions disableSpacing>
                <IconButton aria-label="add to favorites" sx={{ color: red[500] }}>
                  <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                  <ShareIcon />
                </IconButton>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}